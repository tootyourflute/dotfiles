export EDITOR=nvim
export PATH=$PATH:~/.scripts/:~/.scripts/wine/
export HISTFILE=~/.zsh/.zsh_history
export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
export DOCKER_HOST=unix://$XDG_RUNTIME_DIR/docker.sock

HIST_STAMPS="yyyy-mm-dd"

autoload -U compaudit compinit zcalc colors
for x (~/.zsh/*.zsh)
do
	source $x
done

#for x (~/.scripts/*/)
#do
#	export PATH=$PATH:$x
#done

compinit -i -d ~/.zsh/.zcompdump
bindkey -v
export KEYTIMEOUT=1
