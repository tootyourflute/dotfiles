#!/bin/bash

sudo pacman -S i3-wm alacritty firefox mpv discord thunar thunar-volman libreoffice conky polybar qt5-tools dunst libnotify alsa-utils libpulse scrot openvpn curl wget feh pulseaudio jq rofi picom neofetch neovim deluge xorg-setxkbmap xorg-xinput numlockx xorg-xset blueman

yay -S betterlockscreen bluez-hcitool electronmail-bin mato-icons spotify vimix-gtk-themes
