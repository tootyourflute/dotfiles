#!/bin/bash

# Basically a script to handle media keys on my personal laptop
# TODO:
#	Test with other systems
#	Add support for more music players

bright_dir='/sys/class/backlight/intel_backlight'
bright_max=`cat $bright_dir/max_brightness`
bright_cur=`cat $bright_dir/brightness`
bright_inc=`expr $bright_max / 20`

#cmus_status=`cmus-remote -Q | awk -F\  /status/'{print $2}'`

case "$1" in
	-1)
		amixer sset Master toggle
		exit 0
		;;
	-2)
		pactl set-sink-volume 0 -5%
		exit 0
		;;
	-3)
		pactl set-sink-volume 0 +5%
		exit 0
		;;
	-4)
		qdbus org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Previous
		exit 0
		;;
	-5)
		qdbus org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.PlayPause
		exit 0
		;;
	-6)
		qdbus org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Next
		exit 0
		;;
	-11)
		if [ $bright_cur == '0' -o `expr $bright_cur - $bright_inc` == '0' ]
		then
			bright_cur='0'
		else
			let "bright_cur -= bright_inc"
		fi

		echo $bright_cur > $bright_dir/brightness
		exit 0
		;;
	-12)
		if [ $bright_cur == $bright_max -o `expr $bright_cur + $bright_inc` == $bright_max ]
		then
			bright_cur=$bright_max
		else
			let "bright_cur += bright_inc"
		fi
		
		echo $bright_cur > $bright_dir/brightness
		exit 0
		;;
	*)
		echo "You done fucked up fam"
		exit 1
esac
