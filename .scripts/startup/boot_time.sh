#!/bin/bash

boot_time=`systemd-analyze | awk -F ' = ' 'NR==1 {print $2}'`

sleep 1
notify-send -u normal "Boot Time" "$boot_time"
