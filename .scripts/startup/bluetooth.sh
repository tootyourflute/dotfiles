#!/bin/bash

bt_status=`systemctl status bluetooth | awk /Active/'{print $2}'`

if [ "$bt_status" == "inactive" ]
then
	systemctl start bluetooth
	exit 0
elif [ "$bt_status" == "active" ]
then
	systemctl stop bluetooth 
	exit 0
fi
