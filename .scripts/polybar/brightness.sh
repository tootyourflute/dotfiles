#!/bin/bash

bright_dir='/sys/class/backlight/intel_backlight'
bright_max=`cat $bright_dir/max_brightness`
bright_cur=`cat $bright_dir/brightness`
bright_per=$(("$bright_cur * 100 / $bright_max"))

echo " $bright_per%"
