#!/bin/bash

blue_status=`hcitool con | awk 'NR==2 {print}'`

if [ `systemctl status bluetooth | awk /Active/'{print $2}'` == 'inactive' ]
then
	echo -e %{F#f00}
else
	if [ "${blue_status:-0}" != 0 ]
	then
		echo -e %{F#0f0}
	else
		echo -e %{F#0f0}
	fi
fi
