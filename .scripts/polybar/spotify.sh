#!/bin/bash

config_dir="${XDG_CONFIG_HOME:-${HOME}/.config}/spotify_parser"

#Static
client_id=00a2aa401ac44c96aa5fa01e195babde
client_secret=5463df15458a4058a152ff8868b179b3
b64client="MDBhMmFhNDAxYWM0NGM5NmFhNWZhMDFlMTk1YmFiZGU6NTQ2M2RmMTU0NThhNDA1OGExNTJmZjg4NjhiMTc5YjM="
refresh_endpoint="https://accounts.spotify.com/api/token"
play_endpoint="https://api.spotify.com/v1/me/player/currently-playing"
auth='https://accounts.spotify.com/en/authorize?client_id=00a2aa401ac44c96aa5fa01e195babde&response_type=code&scope=user-read-currently-playing&redirect_uri=https:%2F%2Fgoogle.com'

#Check for config file
check_conf() {
if [ -f $config_dir/config ]
	then
		source "$config_dir/config"
	else
		init
	fi
}

#Check if Spotify is running through dbus
check_proc() {
	process=`qdbus | grep spotify`
	if [ -z "$process" ]
	then
		echo "Spotify is not running"
		exit
	fi
}

refresh() {
	output=`curl -s -H "Authorization: Basic $b64client" -d grant_type=refresh_token -d "refresh_token=$ref_token" $refresh_endpoint`
	acc_token=`echo $output | jq 'getpath(["access_token"])'`

	ref_time=`date -d "${date} + 3300 seconds" +%s`

	if [ ! -d $config_dir ]
	then
		mkdir -p $config_dir
	fi
	printf "code=$code\nacc_token=$acc_token\nref_token=$ref_token\n\nref_time=$ref_time" > $config_dir/config
	check_conf
}

grab_data() {
	data=`curl -s "$play_endpoint" -H "Authorization: Bearer $acc_token"`
	if [ -z "$data" ]
	then
		echo "Spotify is not playing"
		exit
	fi
}

parse() {
	position=`echo $(("$(echo $data | jq 'getpath(["progress_ms"])') / 1000")) | bc`
	length=`echo $(("$(echo $data | jq 'getpath(["item"]) | getpath(["duration_ms"])') / 1000")) | bc`
	artist=`echo $data | jq 'getpath(["item"]) | getpath(["artists"]) | .[0] | getpath(["name"])' | tr -d \"` #0.002
	title=`echo $data | jq 'getpath(["item"]) | getpath(["name"])' | tr -d \"` #0.002
	pb_status=`echo $data | jq 'getpath(["is_playing"])'`
	pos_min=`echo $(("$position / 60")) | bc`
	pos_sec=`echo $(("$position % 60")) | bc`
	dur_min=`echo $(("$length / 60")) | bc`
	dur_sec=`echo $(("$length % 60")) | bc`

	if [ "$pos_sec" -le 9 ]; then
		pos_sec=`echo 0$pos_sec`
	fi
	if [ "$dur_sec" -le 9 ]; then
		dur_sec=`echo 0$dur_sec`
	fi

	if [ "$pb_status" = "true" ]; then
		icon=' '
	elif [ "$pb_status" = "false" ]; then
		icon=' '
	fi
}

while :
do
	check_proc # 0.045
	check_conf # 0.000
	#Refresh access token if needed
	cur_date=`date +%s`
	if [ $cur_date -ge $ref_time ]
	then
		refresh
	fi
	grab_data # 0.200
	parse 	  # 0.245

	echo "$icon $artist - $title $pos_min:$pos_sec/$dur_min:$dur_sec"
	sleep 0.6
done
