#!/bin/bash

# $lid_dir is also specific to my system
lid_dir='/proc/acpi/button/lid/LID0'
lid_state=`awk '{print $2}' $lid_dir/state`
# $bright_dir is specific to my system, could change for different systems
bright_dir='/sys/class/backlight/intel_backlight'
bright_cur=`cat $bright_dir/brightness`
# Edit $bright_store to whatever file you want to store to
bright_store="/home/toot/.local/share/brightness"
bright_dim='0'

while true
do
	case "$lid_state" in
		open)
			if [ $bright_cur == $bright_dim ]
			then
				cat $bright_store > $bright_dir/brightness
			fi
			until [ $lid_state == 'closed' ]
			do
				bright_cur=`cat $bright_dir/brightness`
				lid_state=`awk '{print $2}' $lid_dir/state`
				sleep 0.1
			done
			echo $bright_cur > $bright_store
			echo $bright_dim > $bright_dir/brightness
			continue
			;;
		closed)
			until [ $lid_state == 'open' ]
			do
				lid_state=`awk '{print $2}' $lid_dir/state`
				sleep 0.1
			done
			cat $bright_store > $bright_dir/brightness
			bright_cur=`cat $bright_store`
			continue
			;;
	esac
done
