#!/bin/bash

ip_check
cityid=
apikey=
data=`curl -s "https://api.openweathermap.org/data/2.5/weather?units=imperial&id=$cityid&appid=$apikey"`
temp=`echo $data | jq 'getpath(["main"]) | getpath(["temp"])'`
icon=`echo $data | jq 'getpath(["weather"]) | .[0] | getpath(["icon"])' | tr -d \"`
icon_url="https://openweathermap.org/img/wn/$icon@2x.png"

curl -s -o /tmp/w_icon.png "$icon_url"

echo $temp°F
