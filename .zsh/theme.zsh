ZSH_THEME_GIT_PROMPT_PREFIX=" [%{%B%F{blue}%}"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{%f%b%B%F{green}%}]"
ZSH_THEME_GIT_PROMPT_DIRTY=" %{%F{red}%}*%{%f%b%}"
ZSH_THEME_GIT_PROMPT_CLEAN=""

PROMPT='%{%f%b%}
%{$fg_bold[blue]%}> %{$fg[red]%}%~%{%B%F{green}%}$(git_prompt_info)%E%{%f%b%}
%{$fg_bold[blue]%}% %#%{%f%b%} '

RPROMPT=''
