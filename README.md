# dotfiles from laptop

## Programs

- **OS**: Arch Linux
- **Bootloader:** rEFInd
- **Login Screen:** lightdm-gtk-greeter
- **Window Manager:** i3-gaps
- **Lock Screen:** betterlockscreen
- **Status Bar:** polybar
- **App Launcher:** rofi
- **Terminal:** alacritty
- **File Manager:** Thunar
- **Shell:** zsh
- **Theme:** vimix-dark
- **Icons:** Mato
- **Font:** Fira Mono

## Workspace Layout

| Workspace |    Programs    |
|:---------:|:--------------:|
|     1     |    firefox     |
|     2     |    terminal    |
|     3     |  video player  |
|     4     |    spotify     |
|     5     |    discord     |
|     6     |  file manager  |
|     7     |  libreoffice   |
|     8     | torrent client |
|     9     |  electronmail  |
|     0     |    blueman     |

## Screenshots

![rEFInd](https://gitlab.com/tootyourflute/laptop-dotfiles/-/raw/main/screenshots/refind.png?ref_type=heads)
![lightdm](https://gitlab.com/tootyourflute/laptop-dotfiles/-/raw/main/screenshots/lightdm.png?ref_type=heads)
![i3wm](https://gitlab.com/tootyourflute/laptop-dotfiles/-/raw/main/screenshots/i3wm-polybar.png?ref_type=heads)
![neovim](https://gitlab.com/tootyourflute/laptop-dotfiles/-/raw/main/screenshots/nvim.png?ref_type=heads)
![rofi](https://gitlab.com/tootyourflute/laptop-dotfiles/-/raw/main/screenshots/rofi.png?ref_type=heads)
![betterlockscreen](https://gitlab.com/tootyourflute/laptop-dotfiles/-/raw/main/screenshots/betterlockscreen.png?ref_type=heads)
